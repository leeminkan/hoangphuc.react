import * as AuthConstants from '../../containers/Auth/Constants';
import * as HomeConstants from '../../containers/Home/Constants';
import * as PostsConstants from '../../containers/Posts/Constants';
import * as UserPageConstants from '../../containers/UserPage/Constants';
import * as ProfileConstants from '../../containers/Profile/Constants';
import * as ErrorConstants from '../../containers/Error/Constants';
import * as NotificationConstants from '../../containers/Notification/Constants';
import * as ConfirmationDialogConstants from '../../containers/ConfirmationDialog/Constants';
import * as FriendsConstants from '../../containers/Friends/Constants';
const types = {
    ...AuthConstants,
    ...HomeConstants,
    ...PostsConstants,
    ...UserPageConstants,
    ...ProfileConstants,
    ...ErrorConstants,
    ...NotificationConstants,
    ...ConfirmationDialogConstants,
    ...FriendsConstants
}
export default types;