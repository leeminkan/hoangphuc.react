import * as AuthActions from '../../containers/Auth/Actions';
import * as HomeActions from '../../containers/Home/Actions';
import * as PostsActions from '../../containers/Posts/Actions';
import * as UserPageActions from '../../containers/UserPage/Actions';
import * as ProfileActions from '../../containers/Profile/Actions';
import * as ErrorActions from '../../containers/Error/Actions';
import * as NotificationActions from '../../containers/Notification/Actions';
import * as ConfirmationDialogActions from '../../containers/ConfirmationDialog/Actions';
import * as FriendsActions from '../../containers/Friends/Actions';
const actions = {
    ...AuthActions,
    ...HomeActions,
    ...PostsActions,
    ...UserPageActions,
    ...ProfileActions,
    ...ErrorActions,
    ...NotificationActions,
    ...ConfirmationDialogActions,
    ...FriendsActions,
}
export default actions;