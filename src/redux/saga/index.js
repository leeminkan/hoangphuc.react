import { all } from 'redux-saga/effects';
import authSaga from '../../containers/Auth/Saga';
import homeSaga from '../../containers/Home/Saga';
import postsSaga from '../../containers/Posts/Saga';
import userPageSaga from '../../containers/UserPage/Saga';
import profileSaga from '../../containers/Profile/Saga';
import notificationSaga from '../../containers/Notification/Saga';
import friendsSaga from '../../containers/Friends/Saga';


export default function* rootSaga() {
  yield all([
    authSaga,
    homeSaga,
    postsSaga,
    userPageSaga,
    profileSaga,
    notificationSaga,
    friendsSaga
  ]);
}