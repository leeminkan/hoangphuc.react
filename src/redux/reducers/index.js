
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import {reducer as formReducer} from 'redux-form';

import AuthReducer from '../../containers/Auth/Reducer';
import HomeReducer from '../../containers/Home/Reducer';
import PostsReducer from '../../containers/Posts/Reducer';
import UserPageReducer from '../../containers/UserPage/Reducer';
import ProfileReducer from '../../containers/Profile/Reducer';
import ErrorReducer from '../../containers/Error/Reducer';
import NotificationReducer from '../../containers/Notification/Reducer';
import ConfirmationDialogReducer from '../../containers/ConfirmationDialog/Reducer';
import FriendsReducer from '../../containers/Friends/Reducer';


export default (history) => combineReducers({
    form: formReducer,
    auth: AuthReducer,
    home: HomeReducer,
    posts: PostsReducer,
    userPage: UserPageReducer,
    profile: ProfileReducer,
    error: ErrorReducer,
    notification: NotificationReducer,
    confirmationDialogReducer: ConfirmationDialogReducer,
    friends: FriendsReducer,
    router: connectRouter(history),
  })
