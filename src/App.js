import React from 'react';
import './App.css';
import {  Switch, Route } from 'react-router-dom';
import Navbar from './component/Nav/Navbar';
import PrivateRoute from './component/RouteCustom/PrivateRoute';

import Test from './containers/Test';
import Home from './containers/Home';
import UserPage from './containers/UserPage';
import FriendPage from './containers/Friends';
import SearchPage from './containers/SearchPage';
import Auth from './containers/Auth';

import ErrorMessage from './containers/Error';
import NotificationMessage from './containers/Notification';
import ConfirmationDialog from './containers/ConfirmationDialog';

function App() {
  return (
      <div className="App">
      <ErrorMessage/>
      <NotificationMessage/>
      <ConfirmationDialog/>
      <Switch>
          <Route path="/test" component={Test}>
          </Route>
          <Route path="/auth" component={Auth}>
          </Route>
          <Route path="/">
            <Route path="/" component={Navbar}/>
            <PrivateRoute exact path="/" component={Home}/>
            <PrivateRoute path="/user" component={UserPage}/>
            <PrivateRoute path="/friend" component={FriendPage}/>
            <PrivateRoute path="/search" component={SearchPage}/>
          </Route>
      </Switch>
      </div>
  );
}

export default App;
