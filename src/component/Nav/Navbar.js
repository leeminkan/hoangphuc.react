import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Button from '../Form/ButtonField';
import FormSearch from './FormSearch';


import { connect } from 'react-redux';
import actions from '../../redux/actions';


class Navbar extends Component {
    search = (value) => {
        let pathname = '/search/' + value;
        this.props.history.push({
            pathname: pathname,
            value: value
          });
    }
    render() {
        return (
          <div>
            <nav className="navbar navbar-light bg-light justify-content-between">
              <Link className="navbar-brand" to="/">
                ReactBasic
              </Link>
              <FormSearch search={this.search} />
              <div className="partRight">
                <div className="dropdown show">
                    <a
                    className="dropdown-toggle"
                    href="/"
                    id="dropdownMenuLink" 
                    data-toggle="dropdown" 
                    aria-haspopup="true" 
                    aria-expanded="false"
                    >
                        <img
                        className="avater-size-1" 
                        src="/img/ava.gif" 
                        alt="User" 
                        >
                        </img>
                    </a>
                    <div 
                    className="dropdown-menu dropdown-menu-right" 
                    aria-labelledby="dropdownMenuLink">
                        <Link className="dropdown-item" to="/user">
                            User
                        </Link>
                        <Button
                        className= "dropdown-item"
                        type= "button"
                        content= "Log out!"
                        onClick= {this.props.onLogOut}
                        >
                        </Button>
                    </div>
                </div>
              </div>
            </nav>
          </div>
        );
    }
}
const mapDispatchToProps = dispatch => ({
    onLogOut: () =>
    dispatch(actions.logOut())
  });
export default connect(null, mapDispatchToProps)(Navbar);
