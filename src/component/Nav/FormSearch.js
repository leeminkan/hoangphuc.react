import React, { Component } from 'react'


export default class FormSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        };
    }
    handleChange = (e) => {
        let target = e.target;
        let value = target.value;
        this.setState(
            {
                value: value
            }
        );
    }
    onClickSearch = (e) =>
    {
        e.preventDefault();
        return this.props.search(this.state.value);
    }
    render() {
        return (
            <div>
                <form className="form-inline" onSubmit={this.onClickSearch}>
                    <input className="form-control mr-sm-2"
                    type="text"
                    placeholder="Search"
                    value={this.state.value || ''}
                    onChange={this.handleChange}></input>
                    <button
                    className="btn btn-outline-success my-2 my-sm-0" 
                    type="submit"
                    >Search</button>
                </form>
            </div>
        )
    }
}
