import React, { Component } from 'react';

class TextField extends Component {
    render(){
        return (
            <div className="FormField">
                <label className="FormField__Label">{this.props.labelName}</label>
                <input type={this.props.inputType}
                 id={this.props.inputName}
                className="FormField__Input"
                placeholder={this.props.inputPlaceholder}
                name={this.props.inputName}
                value={this.props.value}
                disabled = {(this.props.disabled)? "disabled" : ""}
                onChange={this.props.onChange}
                />
            </div>
        );
    }
}
export default TextField;