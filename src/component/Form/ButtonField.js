import React, { Component } from 'react';

class ButtonField extends Component {
    render(){
        let className = (this.props.className)?this.props.className:"FormField__Button mr-20";
        return (
                <button className= {className}
                type= {this.props.type}
                onClick = {this.props.onClick}
                disabled={this.props.disabled}>
                {this.props.content}
                </button>
        );
    }
}
export default ButtonField;