import React from "react";
import { Field, reset, reduxForm } from "redux-form";
import {
  required,
  minLength6,
  maxLength16,
  passwordsMatch
} from "../../services/validation";
import FieldValidation from "./FieldValidation";

const afterSubmit = (result, dispatch) =>
dispatch(reset('changePasswordForm'));
const ChangePasswordForm = props => {
  const { handleSubmit, submitting, pristine } = props;
  
  return (
    <form onSubmit={handleSubmit}>
      <Field
        id="oldpassword"
        name="oldpassword"
        type="password"
        component={FieldValidation}
        label="Old Password"
        validate={[required]}
      />
      <Field
        id="password"
        name="password"
        type="password"
        component={FieldValidation}
        label="New Password"
        validate={[required, minLength6, maxLength16]}
      />
      <Field
        id="repassword"
        name="repassword"
        type="password"
        component={FieldValidation}
        label="RePassword"
        validate={[required, minLength6, maxLength16, passwordsMatch]}
      />
      <div className="text-center">
        <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>
          Change
        </button>
      </div>
    </form>
  );
};
export default reduxForm({
  form: "changePasswordForm",
  onSubmitSuccess: afterSubmit,
})(ChangePasswordForm);
