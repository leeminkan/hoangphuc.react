import React from 'react';

import './index.css';

// const FieldValidation = ({
//   id,
//   input,
//   label,
//   type,
//   meta: { touched, error, warning }
// }) => (
//   <div className="form-label-group">
//       <input className="form-control"
//         id={id}
//         {...input}
//         placeholder={label}
//         type={type}
//       />
//       <label htmlFor={id}>{label}</label>
//       {touched &&
//         ((error && <span>{error}</span>) ||
//           (warning && <span>{warning}</span>))}
//   </div>
// );
const FieldValidation = ({
  input,
  label,
  type,
  textarea,
  meta: { touched, error, warning, invalid }
}) => {
  const textareaType = (
    <textarea
      rows="4"
      cols="50"
      {...input}
      placeholder={label}
      type={type}
      className={`form-control ${touched && invalid ? "has-danger" : ""}`}
    />
  );
  const inputType = (
    <input
      {...input}
      placeholder={label}
      type={type}
      className={`form-control ${touched && invalid ? "has-danger" : ""}`}
    />
  );

  return (
    <div className="form-label-group">
      {textarea ? textareaType : inputType}
      <label>{label}</label>
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  );
};

export default FieldValidation;