import React from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from 'react-router-dom';
import {
  required,
  minLength6,
  maxLength16,
  passwordsMatch
} from "../../services/validation";
import FieldValidation from "./FieldValidation";

const ResetPasswordForm = props => {
  const { handleSubmit, submitting, pristine } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        id="token"
        name="token"
        type="hidden"
        component="input"
        validate={[required]}
      />
      <Field
        id="password"
        name="password"
        type="password"
        component={FieldValidation}
        label="Password"
        validate={[required, minLength6, maxLength16]}
      />
      <Field
        id="repassword"
        name="repassword"
        type="password"
        component={FieldValidation}
        label="RePassword"
        validate={[required, minLength6, maxLength16, passwordsMatch]}
      />
      <div className="text-center">
        <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>
          Change
        </button>
      </div>
      <div className="text-center">
        <Link className="small" to={"/auth/signin"}>
          Login...
        </Link>
      </div>
    </form>
  );
};
export default reduxForm({
  form: "resetPasswordForm"
})(ResetPasswordForm);
