import React, { Component } from 'react';

class GenderField extends Component {
    render(){
        return (
            <div className="FormField">
                <label className="FormField_GenderLabel" htmlFor="male">
                <input type="radio"
                name="gender"
                id="male"
                value= {1}
                checked={this.props.gender === "1"}
                disabled = {(this.props.disabled)? "disabled" : ""}
                onChange={ this.props.onChange }
                />{" "}
                Male</label>
                <label className="FormField_GenderLabel" htmlFor="female">
                <input
                type="radio" 
                name="gender" 
                id="female"
                value={0}
                checked={this.props.gender === "0"}
                disabled = {(this.props.disabled)? "disabled" : ""}
                onChange={ this.props.onChange }
                />{" "}
                Female</label>
                
            </div>
        );
    }
}
export default GenderField;