import React from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from 'react-router-dom';
import {
  required,
  email
} from "../../services/validation";
import FieldValidation from "./FieldValidation";

const RequestResetPasswordForm = props => {
  const { handleSubmit, submitting, pristine } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        id="email"
        name="email"
        type="email"
        component={FieldValidation}
        label="Email"
        validate={[required, email]}
      />
      <div className="text-center">
        <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>
          Request
        </button>
      </div>
      <div className="text-center">
        <Link className="small" to={"/auth/signin"}>
          Login...
        </Link>
      </div>
    </form>
  );
};
export default reduxForm({
  form: "requestResetPasswordForm"
})(RequestResetPasswordForm);
