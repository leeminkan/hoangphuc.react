import React from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from 'react-router-dom';
import {
  required,
  email
} from "../../services/validation";
import FieldValidation from "./FieldValidation";

const SigninForm = props => {
  const { handleSubmit, submitting } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        id="email"
        name="email"
        type="email"
        component={FieldValidation}
        label="Email"
        validate={[required, email]}
      />
      <Field
        id="password"
        name="password"
        type="password"
        component={FieldValidation}
        label="Password"
        validate={required}
      />
      <div className="text-center">
        <button type="submit" className="btn btn-primary" disabled={submitting}>
          Login
        </button>
      </div>
      <div className="text-center">
        <Link className="small" to={"/auth/signup"}>
          You haven't account!
        </Link>
        {' or '}
        <Link className="small" to={"/auth/request-reset-password"}>
          Forgot password?
        </Link>
      </div>
    </form>
  );
};
export default reduxForm({
  form: "signinForm"
})(SigninForm);
