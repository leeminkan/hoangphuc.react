import React from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import {
  required,
  alphaNumeric,
  email,
  minLength6,
  maxLength16,
  passwordsMatch
} from "../../services/validation";
import FieldValidation from "./FieldValidation";

const SignupForm = props => {
  const { handleSubmit, pristine, submitting } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        id="name"
        name="name"
        type="text"
        component={FieldValidation}
        label="Name"
        validate={[required]}
        warn={alphaNumeric}
      />
      <Field
        id="email"
        name="email"
        type="email"
        component={FieldValidation}
        label="Email"
        validate={[required, email]}
      />
      <Field
        id="password"
        name="password"
        type="password"
        component={FieldValidation}
        label="Password"
        validate={[required, minLength6, maxLength16]}
      />
      <Field
        id="repassword"
        name="repassword"
        type="password"
        component={FieldValidation}
        label="RePassword"
        validate={[required, minLength6, maxLength16, passwordsMatch]}
      />
      <Field
        id="dateOfBirth"
        name="dateOfBirth"
        type="date"
        component={FieldValidation}
        validate={[required]}
        label="Date Of Birth"
      />
      <div className="form-gender">
        <label>
          <Field
            name="gender"
            component="input"
            type="radio"
            value="1"
            checked
          />{" "}
          Male
        </label>
        <label>
          <Field name="gender" component="input" type="radio" value="0" />{" "}
          Female
        </label>
      </div>

      <Field
        id="description"
        name="description"
        type="text"
        component={FieldValidation}
        validate={[required]}
        label="Description"
      />

      <div className="text-center">
        <button
          className="btn btn-primary"
          type="submit"
          disabled={pristine || submitting}
        >
          Register
        </button>
      </div>

      <div className="text-center">
        <Link className="small" to={"/auth/signin"}>
          I'm already member
        </Link>
      </div>
    </form>
  );
};

export default reduxForm({
  form: "signupForm"
})(SignupForm);
