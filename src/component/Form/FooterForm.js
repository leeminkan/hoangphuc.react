import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class FooterForm extends Component {
    render() {
        return (
            <div className="FormField">
                <button className="FormField__Button mr-20">
                    {this.props.btn_content}
                </button>
                <Link className='FormField__Link' to={this.props.a_href}>{this.props.a_content}</Link>
            </div>
        )
    }
}
