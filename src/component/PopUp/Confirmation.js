import React, { Component } from 'react';

import { connect } from 'react-redux';
import './index.css';


import LoadingSpinner from '../LoadingSpinner';

class Confirmation extends Component {
    render(){
        const {show, inProgress} = this.props;
        if (!show) return null;
        return(
            <div className='popup'>
                <div className='popup_inner'>
                    <div className="popup_content">
                        <div className="popup_header">
                            <span>{this.props.title}</span>
                            <button className="close"
                            onClick={this.props.onNo}>
                                <span>×</span>
                            </button>       
                        </div> 
                        <div className="popup_body">
                            {this.props.body}
                        </div>
                        <div className="popup_footer">
                            <div className="popup_loading">
                                {inProgress ? <LoadingSpinner /> : null }
                            </div>
                            <div className="popup_confirm">
                                <button className="btn btn-danger"
                                onClick={this.props.onYes}>
                                {this.props.yesText||'Yes'}
                                </button>
                                <button className="btn btn-primary"
                                onClick={this.props.onNo}>
                                {this.props.noText||'No'}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state.confirmationDialogReducer
});
export default connect(mapStateToProps, null)(Confirmation);
