import React, { Component } from 'react';

import './index.css';

class MessageRedirect extends Component {
    renderArray = () =>{
        let body = this.props.body;
        return body.map((item, index) => {
            return (
                <li key = {index}>
                    {item.errorMessage||item.notificationMessage}
                </li>
            )
        });
    }
    render(){
        return(
            <div className='popup'>
                <div className='popup_inner'>
                    <div className="popup_content">
                        <div className="popup_header">
                            <span>{this.props.title}</span>
                            <button className="close"
                            onClick={this.props.onClosePopup}>
                                <span>×</span>
                            </button>       
                        </div> 
                        <div className="popup_body">
                            {this.renderArray()}
                        </div>
                        <div className="popup_footer">
                            <button className="btn btn-secondary"
                            onClick={this.props.onClosePopup}>
                            Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default MessageRedirect;
