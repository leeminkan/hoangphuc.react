import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import serviceAuth from '../../services/serviceAuth';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        serviceAuth.auth() === true
        ? <Component {...props} />
        : <Redirect to={{
            pathname: '/auth/signin',
            state: { from: props.location }
          }} />
    )} />
  )
export default PrivateRoute;
