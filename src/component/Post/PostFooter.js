import React, { Component } from 'react';
import ButtonField from '../Form/ButtonField';

class PostFooter extends Component {
    render(){
        return(
            <div className="cardbox-comments">
                <input placeholder="Write a comment" type="text"></input>
                <ButtonField
                className="btn btn-success"
                content = "Send"></ButtonField>
			 </div>
        );
    }
}
export default PostFooter;
