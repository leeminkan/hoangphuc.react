import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import actions from '../../redux/actions';

class PostHeading extends Component {
  render() {
    return (
      <div className="cardbox-heading">
        <div className="media m-0">
          <div className="d-flex mr-3">
            <Link to="/user">
              <img
                className="img-fluid rounded-circle"
                src="/img/ava.gif"
                alt="User"
              />
            </Link>
          </div>
          <div className="media-body">
            <p className="m-0"> {this.props.name}</p>
            <small>
              <span>
                <i className="icon ion-md-pin" /> Nairobi, Kenya
              </span>
            </small>
            <small>
              <span>
                <i className="icon ion-md-time" />
                {this.props.updatedAt}
              </span>
            </small>
          </div>

          <div className="dropdown show">
            <button
              className="btn btn-link"
              type="button"
              id="gedf-drop1"
              data-toggle="dropdown"
              aria-haspopup="false"
              aria-expanded="false"
            >
              <i className="fa fa-ellipsis-h" />
            </button>
            <div
              className="dropdown-menu dropdown-menu-right"
              aria-labelledby="gedf-drop1"
              x-placement="bottom-end"
            >
              <div className="h6 dropdown-header">Configuration</div>
              <a className="dropdown-item" href="/">
                Edit
              </a>
              <button
                onClick={() => this.props.deletePost(this.props.index)}
                className="dropdown-item"
              >
                Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
    ...state.auth
});
const mapDispatchToProps = dispatch => ({
    deletePost: (id) =>
    dispatch(actions.deletePost(id))
}
);
export default connect(mapStateToProps, mapDispatchToProps)(PostHeading);
