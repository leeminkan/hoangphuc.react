import React, { Component } from 'react';
import PostHeading from './PostHeading';
import PostContent from './PostContent';
import PostFooter from './PostFooter';

class Post extends Component {
    render(){
        const { className } = this.props;
        return(
            <div className="row">
                <div className={className}>
                    <div className="cardbox shadow-lg bg-white">
                        <PostHeading
                        index={this.props.index}
                        name={this.props.name}
                        updatedAt={this.props.updatedAt}></PostHeading>
                        <PostContent
                        content = {this.props.content}
                        img = {this.props.img}
                        ></PostContent>
                        <PostFooter></PostFooter>
                    </div>
                </div>
            </div>
        );
    }
}

export default Post;
