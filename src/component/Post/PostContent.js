import React, { Component } from 'react';

class PostContent extends Component {
    render(){
        return(
            <div className="cardbox-item">
                <p>
                {this.props.content}
                </p>    
                {this.props.img?<img className="img-fluid" src={this.props.img} alt="PostImage"/>:""}            
            </div>
        );
    }
}
export default PostContent;
