
const Error = [
    ["name", {
        regexp: /^[a-zA-Z0-9]{3,}$/,
        errorMessage: 'Name is incorrect!'
    }],
    ["email", {
        regexp: /^[a-z][a-z0-9]{2,}@[a-z0-9]{2,}(\.[a-z0-9]{2,}){1,2}$/,
        errorMessage: 'Email is incorrect'
    }],
    ["password", {
        regexp: /^[a-zA-Z0-9]{6,16}$/,
        errorMessage: 'Password is incorrect'
    }],
    ["repassword", {
        regexp: /^[a-zA-Z0-9]{6,16}$/,
        errorMessage: 'Repassword is incorrect'
    }],
    ["description", {
        regexp: /^(\w+\S+)$/,
        errorMessage: 'Description is incorrect'
    }],
];

const findValidation = (inputName) => {
    return Error.find(function(element) {
        return element[0] === inputName;
      });
}

const validateInput = (inputName, checkingText) => {
    let valitation = findValidation(inputName);
    if (!valitation) return {
        isInputValid: true,
        errorMessage: ''
    };
    const regexp = valitation[1].regexp; 
    if (regexp.exec(checkingText) !== null) {
            return {
                isInputValid: true,
                errorMessage: ''
            };
        } else {
            return {
                isInputValid: false,
                errorMessage: valitation[1].errorMessage
            };
        }
}
export default validateInput;