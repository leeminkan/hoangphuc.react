const host = "http://laravel.basic/api";
const api = {
  signIn: host + "/auth/signin",
  signUp: host + "/auth/signup",
  requestResetPassword: host + "/auth/password/request-reset-password",
  resetPassword: host + "/auth/password/reset-password",
  changePassword: host + "/auth/change-password",
  getProfile: host + "/user/profile",
  updateProfile: host + "/user/update-profile",
  getNewFeed: host + "/user/new-feed",
  getMyPost: host + "/user/my-post",
  createPost: host + "/post/create",
  deletePost: host + "/post/delete",
  searchPost: host + "/user/search-post",
  listFriends: host + "/user/list-friends",
}
export {api, host};
