import React, { Component, Fragment } from 'react';

import LoadingSpinner from '../../component/LoadingSpinner';

import { connect } from 'react-redux';
import * as actions from './Actions';

import RequestResetPasswordForm from '../../component/Form/RequestResetPasswordForm';

class RequestResetPassword extends Component {
  submit = values => {
    this.props.onSubmit(values);
  };
  render() {
    const { inProgress } = this.props;
    return (
      <Fragment>
        <h3 className="login-heading mb-4 text-center">Request Reset Password!</h3>
        <RequestResetPasswordForm
          onSubmit={this.submit}
        />
        {inProgress ? <LoadingSpinner /> : null }
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
    ...state.auth
});
const mapDispatchToProps = dispatch => ({
    onSubmit: (payload) =>
    dispatch(actions.requestResetPasswordRequesting(payload))
}
);
export default connect(mapStateToProps, mapDispatchToProps)(RequestResetPassword);