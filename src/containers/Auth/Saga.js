import { put, call, takeLatest } from 'redux-saga/effects';

import actions from '../../redux/actions';
import * as types from './Constants';

import {api} from '../../config';
import axios from 'axios';

import{ push }from'connected-react-router';

const signinApi = async (user) => {
  let response = await axios.post(api.signIn, user);
  return response;
}

function* signInFlow(action){
  try {
    const user = action.user;
    let respone = yield call(signinApi, user);
    yield put(actions.signInSuccess(respone.data.data));
    localStorage.setItem('token', respone.data.data.token);
    localStorage.setItem('user', JSON.stringify(respone.data.data.user));
    yield put(push('/'));

  } catch(error){
    if (error.response && error.response.data) {
      let errors = error.response.data.errors;
      yield put(actions.signInError(errors));
      yield put(actions.addError(errors));
    }  else {
      let errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
      yield put(actions.addError(errors));
      yield put(actions.signInError(errors));
    }
  };
}
const signupApi = async (user) => {
  let response = await axios.post(api.signUp, user);
  return response;
}
function* signUpFlow(action){
  try {
    const user = action.user;
    user.gender = (user.gender === '1') ? true : false;
    let respone = yield call(signupApi, user);
    yield put(actions.signInSuccess(respone.data.data));
    localStorage.setItem('token', respone.data.data.token);
    localStorage.setItem('user', JSON.stringify(respone.data.data.user));
    yield put(push('/'));

  } catch(error){
    if (error.response && error.response.data) {
      let errors = error.response.data.errors;
      yield put(actions.signUpError(errors));
      yield put(actions.addError(errors));
    }  else {
      let errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
      yield put(actions.signUpError(errors));
      yield put(actions.addError(errors));
    }
  };
}
function* logOutFlow(){
  try {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    yield put(push('/auth/signin'));
  } catch(error){
    let errors = 
        {
          errorCode: 123456789,
          errorMessage: error
        };
    yield put(actions.addError(errors));
  };
}
const requestResetPasswordApi = async (payload) => {
  let response = await axios.post(api.requestResetPassword, payload);
  return response;
}
function* requestResetPasswordFlow(action){
  try {
    let respone = yield call(requestResetPasswordApi, action.payload);
    yield put(actions.resetPasswordSuccess(respone.data.data));
    let notification = 
        {
          notificationCode: 123456789,
          notificationMessage: "Done, please check your mail!"
        };
    let link = "/auth/signin";
    yield put(actions.addNotificationRedirect(notification,link));

  } catch(error){
    if (error.response && error.response.data) {
      let errors = error.response.data.errors;
      yield put(actions.resetPasswordError(errors));
      yield put(actions.addError(errors));
    }  else {
      let errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
      yield put(actions.resetPasswordError(errors));
      yield put(actions.addError(errors));
    }
  };
}
const resetPasswordApi = async (payload) => {
  let response = await axios.post(api.resetPassword, payload);
  return response;
}
function* resetPasswordFlow(action){
  try {
    debugger;
    let respone = yield call(resetPasswordApi, action.payload);
    yield put(actions.resetPasswordSuccess(respone.data.data));
    let notification = 
        {
          notificationCode: 123456789,
          notificationMessage: "Reset password successfully!"
        };
    yield put(push('/auth/signin'));
    yield put(actions.addNotification(notification));

  } catch(error){
    if (error.response && error.response.data) {
      let errors = error.response.data.errors;
      yield put(actions.resetPasswordError(errors));
      yield put(actions.addError(errors));
    }  else {
      let errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
      yield put(actions.resetPasswordError(errors));
      yield put(actions.addError(errors));
    }
  };
}
function* actionWatcher() {
  yield takeLatest(types.SIGNIN_REQUESTING, signInFlow);
  yield takeLatest(types.SIGNUP_REQUESTING, signUpFlow);
  yield takeLatest(types.REQUEST_RESET_PASSWORD_REQUESTING, requestResetPasswordFlow);
  yield takeLatest(types.RESET_PASSWORD_REQUESTING, resetPasswordFlow);
  yield takeLatest(types.LOGOUT, logOutFlow);
}

export default actionWatcher();