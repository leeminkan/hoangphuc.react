import React, { Component, Fragment } from 'react';

import LoadingSpinner from '../../component/LoadingSpinner';

import { connect } from 'react-redux';
import * as actions from './Actions';

import SigninForm from '../../component/Form/SigninForm';

class SignIn extends Component {
  onSubmit = values => {
    this.props.onSubmit(values);
  };
  render() {
    const { inProgress } = this.props;
    return (
      <Fragment>
        <h3 className="login-heading mb-4 text-center">Sign In!</h3>
        <SigninForm
          onSubmit={this.onSubmit}
        />
        {inProgress ? <LoadingSpinner /> : null }
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
    ...state.auth
});
const mapDispatchToProps = dispatch => ({
    onSubmit: (user) =>
    dispatch(actions.signInRequesting(user))
}
);
export default connect(mapStateToProps, mapDispatchToProps)(SignIn);