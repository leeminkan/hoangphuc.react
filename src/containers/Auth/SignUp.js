import React, { Component, Fragment } from 'react';

import LoadingSpinner from '../../component/LoadingSpinner';

import { connect } from 'react-redux';
import * as actions from './Actions';

import SignupForm from '../../component/Form/SignupForm';

class SignUp extends Component {
  onSubmit = values => {
    this.props.onSubmit(values);
  };
  render() {
    const { inProgress } = this.props;
    return (
      <Fragment>
        <h3 className="login-heading mb-4 text-center">Sign Up!</h3>
        <SignupForm onSubmit={this.onSubmit} />
        {inProgress ? <LoadingSpinner /> : null}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    ...state.auth
});
const mapDispatchToProps = dispatch => ({
    onSubmit: (user) =>
    dispatch(actions.signUpRequesting(user))
}
);
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);