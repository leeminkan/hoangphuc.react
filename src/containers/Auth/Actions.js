import * as types from './Constants';

export const updateFieldAuth = (key, value) => ({
    type: types.UPDATE_FIELD_AUTH,
    key: key,
    value: value
});
export const signInRequesting = (user) => ({
    type: types.SIGNIN_REQUESTING,
    user: user
});
export const signInSuccess = (response) => ({
    type: types.SIGNIN_SUCCESS,
    response: response
});
export const signInError = (errors) => ({
    type: types.SIGNIN_ERROR,
    errors: errors
});

export const signUpRequesting = (user) => ({
    type: types.SIGNUP_REQUESTING,
    user: user
});
export const signUpSuccess = (response) => ({
    type: types.SIGNUP_SUCCESS,
    response: response
});
export const signUpError = (errors) => ({
    type: types.SIGNUP_ERROR,
    errors: errors
});

export const requestResetPasswordRequesting = (payload) => ({
    type: types.REQUEST_RESET_PASSWORD_REQUESTING,
    payload: payload
});
export const requestResetPasswordSuccess = (response) => ({
    type: types.REQUEST_RESET_PASSWORD_SUCCESS,
    response: response
});
export const requestResetPasswordError = (errors) => ({
    type: types.REQUEST_RESET_PASSWORD_ERROR,
    errors: errors
});
export const resetPasswordRequesting = (payload) => ({
    type: types.RESET_PASSWORD_REQUESTING,
    payload: payload
});
export const resetPasswordSuccess = (response) => ({
    type: types.RESET_PASSWORD_SUCCESS,
    response: response
});
export const resetPasswordError = (errors) => ({
    type: types.RESET_PASSWORD_ERROR,
    errors: errors
});
export const logOut = () => ({
    type: types.LOGOUT
});
export const authPageUnLoad = () => ({
    type: types.AUTH_PAGE_UNLOAD,
});