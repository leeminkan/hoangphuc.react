import * as types from './Constants';

var initialState = {
    user : {
        name: "",
        email: "",
        password: "",
        repassword: "",
        dateOfBirth: "",
        gender: "",
        description: "",
    },
    inProgress : false,
    errors: []
}

const myReducer = (state = initialState, action) => {
    
    switch (action.type) {
        case types.UPDATE_FIELD_AUTH:
            return { ...state,
                    user:
                    {
                        ...state.user,
                        [action.key]: action.value
                    }
                 };
        case types.SIGNIN_REQUESTING:
            return { ...state, inProgress: true };
        case types.SIGNIN_SUCCESS:
            return { ...state, response: action.response, inProgress: false };
        case types.SIGNIN_ERROR:
            return { ...state, errors: action.errors, inProgress: false };
        case types.SIGNUP_REQUESTING:
            return { ...state, inProgress: true };
        case types.SIGNUP_SUCCESS:
            return { ...state, response: action.response, inProgress: false };
        case types.SIGNUP_ERROR:
            return { ...state, errors: action.errors, inProgress: false };
        case types.REQUEST_RESET_PASSWORD_REQUESTING:
            return { ...state, inProgress: true };
        case types.REQUEST_RESET_PASSWORD_SUCCESS:
            return { ...state, response: action.response, inProgress: false };
        case types.REQUEST_RESET_PASSWORD_ERROR:
            return { ...state, errors: action.errors, inProgress: false };
        case types.RESET_PASSWORD_REQUESTING:
            return { ...state, inProgress: true };
        case types.RESET_PASSWORD_SUCCESS:
            return { ...state, response: action.response, inProgress: false };
        case types.RESET_PASSWORD_ERROR:
            return { ...state, errors: action.errors, inProgress: false };
        case types.LOGOUT:
            return state;
        case types.AUTH_PAGE_UNLOAD:
            return {...initialState};
        default:
            return state;
    }
}

export default myReducer;