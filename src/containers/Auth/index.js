import React, { Component, Fragment } from 'react';
import { Route } from 'react-router-dom';
import serviceAuth from '../../services/serviceAuth';

import SignInForm from './SignIn';
import SignUp from './SignUp';
import ResetPassword from './ResetPassword';
import RequestResetPassword from './RequestResetPassword';

import './index.css';

import { connect } from 'react-redux';
import * as actions from './Actions';

class Auth extends Component {
    componentWillMount(){
        if (serviceAuth.auth()) {
            this.props.history.push("/");
          }
    }
    componentWillUnmount() {
        this.props.onUnload();
    }
    render(){
        return (
          <Fragment>
            <div className="container-fluid">
              <div className="row no-gutter">
                <div className="d-none d-md-flex col-md-4 col-lg-6 bg-image" />
                <div className="col-md-8 col-lg-6">
                  <div className="login d-flex align-items-center py-5">
                    <div className="container">
                      <div className="row">
                        <div className="col-md-9 col-lg-8 mx-auto">
                          <Route
                            exact
                            path="/auth/signup"
                            component={SignUp}
                          />
                          <Route
                            exact
                            path="/auth/signin"
                            component={SignInForm}
                          />
                          <Route
                            exact
                            path="/auth/request-reset-password"
                            component={RequestResetPassword}
                          />
                          <Route
                            path="/auth/reset-password/:token"
                            component={ResetPassword}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Fragment>
        );
    }
}
const mapStateToProps = state => ({
    ...state.auth
});
const mapDispatchToProps = dispatch => ({
    onUnload: () =>
    dispatch(actions.authPageUnLoad())
}
);
export default connect(mapStateToProps, mapDispatchToProps)(Auth);
