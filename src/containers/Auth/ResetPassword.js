import React, { Component, Fragment } from 'react';

import LoadingSpinner from '../../component/LoadingSpinner';

import { connect } from 'react-redux';
import * as actions from './Actions';

import ResetPasswordForm from '../../component/Form/ResetPasswordForm';

class ResetPassword extends Component {
  submit = values => {
    console.log(values);
    this.props.onSubmit(values);
  };
  render() {
    const { inProgress, match } = this.props;
    const initialValues = {
        token: match.params.token
    }
    return (
      <Fragment>
        <h3 className="login-heading mb-4 text-center">Reset Password!</h3>
        <ResetPasswordForm
          onSubmit={this.submit}
          initialValues={initialValues}
        />
        {inProgress ? <LoadingSpinner /> : null }
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
    ...state.auth
});
const mapDispatchToProps = dispatch => ({
    onSubmit: (payload) =>
    dispatch(actions.resetPasswordRequesting(payload))
}
);
export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);