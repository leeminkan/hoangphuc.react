import * as types from './Constants';

var initialState = {
    posts: [],
    newPost: {
        content: "",
        share: 1
    },
    inProgress: false,
    errors: [],
    deletePost: {
        confirmPopup: false
    }
}
const addPost = (state, post) => {
    let posts = [post].concat(state.posts);
    return { ...state,
            posts: posts,
            newPost: {
                content: "",
                share: 1
            },
            inProgress: false
        };
}
const deletePost = (state, id) => {
    let posts = state.posts.filter(post => post.id !== id); 
    console.log("TCL: deletePost -> posts", posts)
    console.log('delete post id: ', id);
    return { ...state,
            posts: posts,
            newPost: {
                content: "",
                share: 1
            },
            inProgress: false
        };
}
const myReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_NEWS_FEED:
      return { ...state, inProgress: {
        getNewfeed: true
      } };
    case types.GET_NEWS_FEED_SUCCESS:
      return { ...state, posts: action.posts, inProgress: false };
    case types.GET_NEWS_FEED_ERROR:
      return { ...state, errors: action.errors, inProgress: false };

    case types.GET_MY_POSTS:
      return { ...state, inProgress: {
        getMyPost: true
      } };
    case types.GET_MY_POSTS_SUCCESS:
      return { ...state, posts: action.posts, inProgress: false };
    case types.GET_MY_POSTS_ERROR:
      return { ...state, errors: action.errors, inProgress: false };

    case types.UPDATE_FIELD_POST:
      return {
        ...state,
        newPost: {
          ...state.newPost,
          content: action.content
        }
      };
    case types.CREATE_POST:
      return { ...state, inProgress: {
        createPost: true
      } };
    case types.CREATE_POST_SUCCESS:
      return addPost(state, action.post);
    case types.CREATE_POST_ERROR:
      return { ...state, errors: action.errors, inProgress: false };

    case types.DELETE_POST:
      return { ...state, inProgress: {
        deletePost: true
      } };
    case types.DELETE_POST_SUCCESS:
      return deletePost(state, action.id);
    case types.DELETE_POST_ERROR:
      return { ...state, errors: action.errors, inProgress: false };

    case types.RESET_POST:
      return { ...initialState };
    default:
      return state;
  }
};

export default myReducer;