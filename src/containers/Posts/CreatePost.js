import React, { Component } from 'react'

import ButtonField from '../../component/Form/ButtonField';
import LoadingSpinner from '../../component/LoadingSpinner';

import { connect } from 'react-redux';
import * as actions from './Actions';


class CreatePost extends Component {
    handleChange = (e) => {
        let target = e.target;
        let value = target.value;
        this.props.onChange(value);
    }
    createPost = () => {
        return this.props.onCreatePost(this.props.newPost);
    }
    render() {
        const { inProgress, className } = this.props;
        return (
            <div className="row">
                <div className={className}>
                    <div className="create_post">
                        <div className="create_post_header">
                            <p>Create Post</p>
                        </div>
                        <textarea 
                        rows="4" 
                        cols="50"
                        value={this.props.newPost.content}
                        onChange={this.handleChange}></textarea>
                        <ButtonField
                        className="btn btn-primary"
                        content =  
                        {inProgress.createPost ? <LoadingSpinner /> : "Create" }
                        onClick = {this.createPost}></ButtonField>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state.posts
});
const mapDispatchToProps = dispatch => ({
    onChange: (content) => 
    dispatch(actions.updateFieldPost(content)),
    onCreatePost: (post) =>
    dispatch(actions.createPost(post)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);
