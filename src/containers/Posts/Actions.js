import * as types from './Constants';
export const getNewsFeed = () => ({
    type: types.GET_NEWS_FEED
});
export const getNewsFeedSuccess = (posts) => ({
    type: types.GET_NEWS_FEED_SUCCESS,
    posts: posts
});
export const getNewsFeedError = (errors) => ({
    type: types.GET_NEWS_FEED_ERROR,
    errors: errors
});

export const getMyPosts = () => ({
    type: types.GET_MY_POSTS
});
export const getMyPostsSuccess = (posts) => ({
    type: types.GET_MY_POSTS_SUCCESS,
    posts: posts
});
export const getMyPostsError = (errors) => ({
    type: types.GET_MY_POSTS_ERROR,
    errors: errors
});

export const updateFieldPost = (content) => ({
    type: types.UPDATE_FIELD_POST,
    content: content
});

export const createPost = (post) => ({
    type: types.CREATE_POST,
    post: post
});
export const createPostSuccess = (post) => ({
    type: types.CREATE_POST_SUCCESS,
    post: post
});
export const createPostError = (errors) => ({
    type: types.CREATE_POST_ERROR,
    errors: errors
});

export const deletePost = (id) => ({
    type: types.DELETE_POST,
    id: id
});
export const deletePostSuccess = (id) => ({
    type: types.DELETE_POST_SUCCESS,
    id: id
});
export const deletePostError = (errors) => ({
    type: types.DELETE_POST_ERROR,
    errors: errors
});
export const resetPost = () => ({
    type: types.RESET_POST
});
