import { put, call, take, takeLatest, race, delay } from 'redux-saga/effects';

import actions from '../../redux/actions';
import types from '../../redux/constants';

import {api} from '../../config';
import axios from 'axios';


function* catchError(error){
  let errors;
  if (error.response && error.response.data) {
      if (error.response.status === 401)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Token expired!"
          };
          yield put(actions.logOut());
      }
      else
      if (error.response.status === 500)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Error Server!"
          };
      }
      else
      {
        errors = error.response.data.errors;
      }
    }  else {
        errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
    }
    return errors;
}
const getNewsFeedApi = async () => {
  let response = await axios.get(api.getNewFeed,
    {
        headers: {
        "Authorization" : `Bearer ${localStorage.getItem('token')}`
        }
    });
  return response;
}

function* getNewsFeedFlow(){
  try {
    const {response} = yield race({
      response: call(getNewsFeedApi),
      timeout: delay(1000)
    });
    if (response)
    {
      yield put(actions.getNewsFeedSuccess(response.data.data));
    }
    else
    {
      let errors = 
        {
          errorCode: 123456789,
          errorMessage: "Request Time Out"
        };
      yield put(actions.getNewsFeedError(errors));
      yield put(actions.addError(errors));
    }
  } catch(error){
    let errors = yield call(catchError, error);
    yield put(actions.getNewsFeedError(errors));
    yield put(actions.addError(errors));
  };
}
const getMyPostsApi = async () => {
  let response = await axios.get(api.getMyPost,
    {
        headers: {
        "Authorization" : `Bearer ${localStorage.getItem('token')}`
        }
    });
  return response;
}
function* getMyPostsFlow(){
  try {
    const {response} = yield race({
      response: call(getMyPostsApi),
      timeout: delay(1000)
    });
    if (response)
    {
      yield put(actions.getMyPostsSuccess(response.data.data));
    }
    else
    {
      let errors = 
        {
          errorCode: 123456789,
          errorMessage: "Request Time Out"
        };
      yield put(actions.getMyPostsError(errors));
      yield put(actions.addError(errors));
    }
  } catch(error){
    let errors = yield call(catchError, error);
    yield put(actions.getMyPostsError(errors));
    yield put(actions.addError(errors));
  };
}
const createPostApi = async (post) => {
  let response = await axios.post(api.createPost, post,
    {
        headers: {
        "Authorization" : `Bearer ${localStorage.getItem('token')}`
        }
    });
  return response;
}
function* createPostFlow(action){
  try {
    let response = yield call(createPostApi, action.post);
    yield put(actions.createPostSuccess(response.data.data.post));
  } catch(error){
    let errors = yield call(catchError, error);
    yield put(actions.createPostError(errors));
    yield put(actions.addError(errors));
  };
}

const deletePostApi = async id => {
  let payload = { data: { postId: id } };
  const request = axios.create({
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  });
  let response = await request.delete(
  api.deletePost, payload);
  return response;
};
function* confirmSaga(confirmationMessage) {
  yield put(actions.showConfirmation(confirmationMessage));
  const { yes } = yield race({
      yes: take(types.CONFIRM_YES),
      no: take(types.CONFIRM_NO)
  });
  return !!yes;
}
function* deletePostFlow(action){
  try {
    const message = 'Are you sure?';
    const confirmed = yield call(confirmSaga, message);
    if (!confirmed) {
        return;
    }
    yield call(deletePostApi, action.id);
    yield put(actions.hideConfirmation());
    yield put(actions.deletePostSuccess(action.id));
  } catch(error){
    let errors = yield call(catchError, error);
    yield put(actions.hideConfirmation());
    yield put(actions.deletePostError(errors));
    yield put(actions.addError(errors));
  };
}


function* actionWatcher() {
  yield takeLatest(types.GET_NEWS_FEED, getNewsFeedFlow);
  yield takeLatest(types.GET_MY_POSTS, getMyPostsFlow);
  yield takeLatest(types.CREATE_POST, createPostFlow);
  yield takeLatest(types.DELETE_POST, deletePostFlow);
}

export default actionWatcher();