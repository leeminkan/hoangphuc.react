import React, { Component, Fragment } from "react";

import CreatePost from "./CreatePost";
import Post from "../../component/Post";

import { connect } from "react-redux";
import './index.css';

class Posts extends Component {
  renderPosts = () => {
    let posts = this.props.posts;
    return posts.map((post, index) => {
      return (
        <Post
          key={index}
          index={post.id}
          name={post.name}
          updatedAt={post.updated_at}
          content={post.content}
          className="col-lg-12"
        />
      );
    });
  };
  render() {
    return (
      <Fragment>
        <CreatePost className={"col-lg-12"}/>
        {this.renderPosts()}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ...state.posts
});
export default connect(
  mapStateToProps,
  null
)(Posts);
