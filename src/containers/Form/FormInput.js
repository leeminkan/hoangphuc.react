import React, { Component } from 'react';
import FormError from './FormError';
import validateInput from '../../services/validateInput';

class FormInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isInputValid: true,
            errorMessage: ''
        };
    }
    handleInputValidation = () => {
        try{
            const {value, inputName} = this.props;
            const { isInputValid, errorMessage } = validateInput(inputName, value);
            this.setState({
            isInputValid: isInputValid,
            errorMessage: errorMessage
            });
        }catch (err)
        {
            console.log(err);
        }
      }
    render(){
        const {
                labelName, 
                inputType,
                inputName,
                inputPlaceholder,
                value,
                disabled,
                onChange
                } = this.props;
        const {
            isInputValid,
            errorMessage
        } = this.state;
        return (
            <div className="FormField">
                <label className="FormField__Label">{labelName}</label>
                <input type={inputType}
                id={inputName}
                name={inputName}
                className="FormField__Input"
                placeholder={inputPlaceholder}
                value={value}
                disabled = {(disabled)? "disabled" : ""}
                onBlur={this.handleInputValidation}
                onChange={onChange}
                required
                />
                <FormError
                isHidden={isInputValid}
                errorMessage={errorMessage}/>
            </div>
        );
    }
}
export default FormInput;