import * as types from './Constants';

var initialState = {
    show: false,
    inProgress : false
}

const myReducer = (state = initialState, action) => {
    
    switch (action.type) {
      case types.SHOWCONFIRMATION:
        return { ...state, show: true };
      case types.CONFIRM_YES:
        return { ...state, show: true, inProgress: true };
      case types.CONFIRM_NO:
        return { ...state, show: false, inProgress: false };
      case types.HIDECONFIRMATION:
        return { ...state, show: false, inProgress: false };
      default:
        return state;
    }
}

export default myReducer;