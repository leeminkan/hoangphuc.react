import * as types from './Constants';

export const showConfirmation = (confirmationMessage) => ({
    type: types.SHOWCONFIRMATION,
    confirmationMessage: confirmationMessage
});
export const confirmYes = () => ({
    type: types.CONFIRM_YES
});
export const confirmNo = () => ({
    type: types.CONFIRM_NO
});
export const hideConfirmation = () => ({
    type: types.HIDECONFIRMATION
});