import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from './Actions';

import Confirmation from '../../component/PopUp/Confirmation';

class confirmationDialog extends Component {
    render(){
        return(
            <Confirmation
            title='Confirmation'
            onNo={this.props.confirmNo}
            onYes={this.props.confirmYes}
            body={this.props.message||"Are you sure?"}
            />
        );
    }
}

const mapStateToProps = state => ({
    ...state.confirmationDialogReducer
});
const mapDispatchToProps = dispatch => ({
    confirmYes: () =>
    dispatch(actions.confirmYes()),
    confirmNo: () => 
    dispatch(actions.confirmNo()),
  });
export default connect(mapStateToProps, mapDispatchToProps)(confirmationDialog);
