import * as types from './Constants';
export const addNotification = (notification) => ({
    type: types.ADD_NOTIFICATION,
    notification: notification
});
export const addNotificationRedirect = (notification, link) => ({
    type: types.ADD_NOTIFICATION_REDIRECT,
    notification: notification,
    link: link
});
export const onRedirect = (link) => ({
    type: types.ON_REDIRECT,
    link: link
});
export const setNotification = (notifications) => ({
    type: types.SET_NOTIFICATION,
    notifications: notifications
});
export const resetNotification = () => ({
    type: types.RESET_NOTIFICATION
});
