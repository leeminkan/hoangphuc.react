import { put , takeLatest } from 'redux-saga/effects';

import actions from '../../redux/actions';
import * as types from './Constants';

import{ push }from'connected-react-router';
function* onRedirectFlow(action){
    try {
        console.log(action.link);
        yield put(push(action.link));
    } catch(error){
        let errors = 
          {
            errorCode: 123456789,
            errorMessage: "Error in Server!"
          };
        yield put(actions.addError(errors));
      }
}
function* actionWatcher() {
    yield takeLatest(types.ON_REDIRECT, onRedirectFlow);
  }
  
  export default actionWatcher();