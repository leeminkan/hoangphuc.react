import * as types from './Constants';

var initialState = {
    notifications: [],
    link: ""
}
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_NOTIFICATION:
            return { ...state,
                notifications: state.notifications.concat(action.notification)
                };
        case types.ADD_NOTIFICATION_REDIRECT:
            return { ...state,
                notifications: state.notifications.concat(action.notification),
                link: action.link
                };
        case types.ON_REDIRECT:
            return initialState;
        case types.SET_NOTIFICATION:
            return { ...state,
                notifications: state.notifications.concat(action.notifications)
                };
        case types.RESET_NOTIFICATION:
            return initialState;
        default:
            return state;
    }
}

export default myReducer;