import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from './Actions';

import PopUp from '../../component/PopUp';
import MessageRedirectComponent from '../../component/PopUp/MessageRedirect';


class Notification extends Component {
    onRedirect = () => {
        this.props.redirect(this.props.link);
    }
    render(){
        if (!Array.isArray(this.props.notifications) || !this.props.notifications.length) {
            // array does not exist, is not an array, or is empty 
            return null;
        }
        if (this.props.link === "")
        {
            return(
                <PopUp
                title='Notification'
                onClosePopup={this.props.reset}
                body={this.props.notifications}
                />
            );
        }
        return(
            <MessageRedirectComponent
            title='Notification'
            onClosePopup={this.onRedirect}
            body={this.props.notifications}
            />
        );

    }
}

const mapStateToProps = state => ({
    ...state.notification
});
const mapDispatchToProps = dispatch => ({
    reset: () =>
    dispatch(actions.resetNotification()),
    redirect: (link) => 
    dispatch(actions.onRedirect(link)),
  });
export default connect(mapStateToProps, mapDispatchToProps)(Notification);
