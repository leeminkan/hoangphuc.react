import React, { Component, Fragment } from 'react';
import './index.css';
class Test extends Component {
  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-8 offset-lg-2">
            <div className="cardbox shadow-lg bg-white">
              <div className="cardbox-heading">
                <div className="media m-0">
                  <div className="d-flex mr-3">
                    <a href="/user">
                      <img
                        className="img-fluid rounded-circle"
                        src="/img/ava.gif"
                        alt="User"
                      />
                    </a>
                  </div>
                  <div className="media-body">
                    <p className="m-0"> Admin</p>
                    <small>
                      <span>
                        <i className="icon ion-md-pin" /> Nairobi, Kenya
                      </span>
                    </small>
                    <small>
                      <span>
                        <i className="icon ion-md-time" />
                        2019-08-02 04:44:41
                      </span>
                    </small>
                  </div>
                  <div className="dropdown show">
                    <button
                      className="btn btn-link"
                      type="button"
                      id="gedf-drop1"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="true"
                    >
                      <i className="fa fa-ellipsis-h" />
                    </button>
                    <div
                      className="dropdown-menu dropdown-menu-right show dropdown-menu-custom"
                      aria-labelledby="gedf-drop1"
                      x-placement="bottom-end"
                    >
                      <div className="h6 dropdown-header">
                        Configuration
                      </div>
                      <a className="dropdown-item" href="/">
                        Edit
                      </a>
                      <a className="dropdown-item" href="/">
                        Delete
                      </a>
                      <button className="dropdown-item">Button</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="cardbox-item">
                <p>
                  In your NavBar.js you don't need to do this.loggedIn =
                  this.props.loggedIn in your constructor, that can lead to
                  problems. You can just access!!
                </p>
              </div>
              <div className="cardbox-comments">
                <input placeholder="Write a comment" type="text" />
                <button className="btn btn-success">Send</button>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default Test;
