import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from './Actions';

import PopUp from '../../component/PopUp';

class Error extends Component {
    render(){
        if (!Array.isArray(this.props.errors) || !this.props.errors.length) { 
            // array does not exist, is not an array, or is empty 
            return null;
        } 
        return(
            <PopUp
            title='Error Message'
            onClosePopup={this.props.onClosePopup}
            body={this.props.errors}
            />
        );
    }
}

const mapStateToProps = state => ({
    ...state.error
});
const mapDispatchToProps = dispatch => ({
    onClosePopup: () =>
    dispatch(actions.resetError()),
  });
export default connect(mapStateToProps, mapDispatchToProps)(Error);
