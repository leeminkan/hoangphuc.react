import * as types from './Constants';
export const addError = (error) => ({
    type: types.ADD_ERROR,
    error: error
});
export const setError = (errors) => ({
    type: types.SET_ERROR,
    errors: errors
});
export const resetError = () => ({
    type: types.RESET_ERROR
});
