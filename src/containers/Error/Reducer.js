import * as types from './Constants';

var initialState = {
    errors: []
}
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_ERROR:
            return { ...state,
                    errors: state.errors.concat(action.error)
                };
        case types.SET_ERROR:
            return { ...state,
                    errors: state.errors.concat(action.error)
                };
        case types.RESET_ERROR:
            return initialState;
        default:
            return state;
    }
}

export default myReducer;