import * as types from './Constants';

var initialState = {
    inProgress : false,
    errors: []
}

const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.HOME_PAGE_LOADED:
            return { ...state };
        case types.HOME_PAGE_UNLOADED:
            return { ...initialState };
        default:
            return state;
    }
}

export default myReducer;