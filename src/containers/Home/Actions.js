import * as types from './Constants';
export const homePageLoaded = () => ({
    type: types.HOME_PAGE_LOADED
});
export const homePageUnloaded = () => ({
    type: types.HOME_PAGE_UNLOADED
});
