import { put, takeLatest } from 'redux-saga/effects';

import actions from '../../redux/actions';
import types from '../../redux/constants';

function* getHomePageFlow(){
  try {
    yield put(actions.getNewsFeed());
  } catch(error){
      let errors = 
      {
        errorCode: 123456789,
        errorMessage: "Error in Server!"
      };
      yield put(actions.addError(errors));
  };
}
function* unloadHomePageFlow(){
  try {
    yield put(actions.resetPost());
  } catch(error){
    let errors = 
    {
      errorCode: 123456789,
      errorMessage: "Error in Server!"
    };
    yield put(actions.addError(errors));
  };
}
function* actionWatcher() {
  yield takeLatest(types.HOME_PAGE_LOADED, getHomePageFlow);
  yield takeLatest(types.HOME_PAGE_UNLOADED, unloadHomePageFlow);
}

export default actionWatcher();