import React, { Component } from 'react';

import Posts from '../Posts';


import { connect } from 'react-redux';
import * as actions from './Actions';


class Home extends Component {
    componentWillMount() {
        this.props.onLoad();
    }
    componentWillUnmount() {
        this.props.onUnload();
    }
    render(){
        return(
            <div className="container">
                <div className="home">
                    <Posts/>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onLoad: () =>
    dispatch(actions.homePageLoaded()),
    onUnload: () =>
    dispatch(actions.homePageUnloaded())
});
export default connect(null, mapDispatchToProps)(Home);
