import { put, call, takeLatest } from 'redux-saga/effects';

import actions from '../../redux/actions';
import types from '../../redux/constants';

import {api} from '../../config';
import axios from 'axios';

function* catchError(error){
  let errors;
  if (error.response && error.response.data) {
      if (error.response.status === 401)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Token expired!"
          };
          yield put(actions.logOut());
      }
      else
      if (error.response.status === 500)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Error Server!"
          };
      }
      else
      { 
        errors = error.response.data.errors;
      }
    }  else {
        errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
    }
    return errors;
}
const getProfileApi = async () => {
    let response = await axios.get(api.getProfile,
        {
            headers: {
            "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        });
    return response;
}
function* getProfileFlow(){
  try {
    let response = yield call(getProfileApi);
    let profile = {
        ...response.data.data,
        gender: response.data.data.gender.toString()
    }
    yield put(actions.getProfileSuccess(profile));
  } catch(error){
    let errors = yield call(catchError, error);
    yield put(actions.getProfileError(errors));
    yield put(actions.addError(errors));
  };
}
const changeProfileApi = async (profile) => {
    let response = await axios.put(api.updateProfile, profile,
        {
            headers: {
            "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        });
    return response;
}
function* changeProfileFlow(action){
    try {
        yield call(changeProfileApi,action.profile);
        let notification = 
        {
          notificationCode: 123456789,
          notificationMessage: "Change Profile Success!"
        };
        yield put(actions.addNotification(notification));
        yield put(actions.changeProfileSuccess(action.profile));
    } catch(error){
      let errors = yield call(catchError, error);
      yield put(actions.changeProfileError(errors));
      yield put(actions.addError(errors));
    };
  }
function* actionWatcher() {
  yield takeLatest(types.GET_PROFILE, getProfileFlow);
  yield takeLatest(types.CHANGE_PROFILE, changeProfileFlow);
}

export default actionWatcher();