import * as types from './Constants';
export const getProfile = () => ({
    type: types.GET_PROFILE
});
export const getProfileSuccess = (user) => ({
    type: types.GET_PROFILE_SUCCESS,
    user: user
});
export const getProfileError = (errors) => ({
    type: types.GET_PROFILE_ERROR,
    errors: errors
});

export const changeEdit = () => ({
    type: types.CHANGE_EDIT
});
export const updateFieldProfile = (key, value) => ({
    type: types.UPDATE_FIELD_PROFILE,
    key: key,
    value: value
});

export const changeProfile = (profile) => ({
    type: types.CHANGE_PROFILE,
    profile: profile
});
export const changeProfileSuccess = (profile) => ({
    type: types.CHANGE_PROFILE_SUCCESS,
    profile: profile
});
export const changeProfileError = (errors) => ({
    type: types.CHANGE_PROFILE_ERROR,
    errors: errors
});