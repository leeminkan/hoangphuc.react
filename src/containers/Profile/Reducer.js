import * as types from './Constants';

var initialState = {
    user: {},
    profile: {},
    edit: false,
    inProgress : false,
    errors: []
}
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_PROFILE:
            return { ...state,
                    inProgress: true
                 };
        case types.GET_PROFILE_SUCCESS:
            return { ...state,
                    user: action.user,
                    profile: action.user,
                    inProgress: false
                    };
        case types.GET_PROFILE_ERROR:
            return { ...state, errors: action.errors, inProgress: false };
        case types.CHANGE_EDIT:
            return { ...state,
                    edit: !state.edit
                    };
        case types.UPDATE_FIELD_PROFILE:
            return { ...state,
                    profile:
                    {
                        ...state.profile,
                        [action.key]: action.value
                    }
                    };
        case types.CHANGE_PROFILE:
            return { ...state,
                    inProgress: true
                    };
        case types.CHANGE_PROFILE_SUCCESS:
            return { ...state,
                    inProgress: false,
                    user: action.profile,
                    edit: false
                    };
        case types.CHANGE_PROFILE_ERROR:
            return { ...state, errors: action.errors, inProgress: false };
        default:
            return state;
    }
}

export default myReducer;