import React, { Component } from "react";

import TextField from "../../component/Form/TextField";
import GenderField from "../../component/Form/GenderField";
import ButtonField from "../../component/Form/ButtonField";

import { connect } from "react-redux";
import * as actions from "./Actions";

import './index.css';

class Profile extends Component {
  handleChange = e => {
    let target = e.target;
    let value = target.type === "checkbox" ? target.checked : target.value;
    let name = target.name;
    this.props.onChangeField(name, value);
  };
  changeEdit = () => {
    this.props.onChangeEdit();
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.onChangeProfile(this.props.profile);
  };
  render() {
    const { profile, edit } = this.props;
    return (
      <div className="content">
        <div className="content-header">Profile</div>
        <form className="FormFields" onSubmit={this.handleSubmit}>
          <TextField
            labelName="Name"
            inputType="text"
            inputName="name"
            value={profile.name || ""}
            disabled={!edit}
            onChange={this.handleChange}
          />
          <TextField
            labelName="E-Mail Address"
            inputType="email"
            inputName="email"
            value={profile.email || ""}
            disabled={true}
            onChange={this.handleChange}
          />
          <TextField
            labelName="Day Of Birth"
            inputType="date"
            inputName="dateOfBirth"
            value={profile.dateOfBirth || ""}
            disabled={!edit}
            onChange={this.handleChange}
          />
          <GenderField
            gender={profile.gender}
            disabled={!edit}
            onChange={this.handleChange}
          />
          <TextField
            labelName="Description"
            inputType="textarea"
            inputName="description"
            value={profile.description || ""}
            disabled={!edit}
            onChange={this.handleChange}
          />
          <div className="profile-form">
            <ButtonField
              type="button"
              onClick={this.changeEdit}
              content={!edit ? "Edit" : "Cancel"}
            />
            <ButtonField
              onClick={this.update}
              className={!edit ? "FormField__Button mr-20 background-gray" : ""}
              disabled={!edit ? true : false}
              content="Update"
            />
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.profile
});
const mapDispatchToProps = dispatch => ({
  onChangeEdit: () => dispatch(actions.changeEdit()),
  onChangeField: (key, value) =>
    dispatch(actions.updateFieldProfile(key, value)),
  onChangeProfile: profile => dispatch(actions.changeProfile(profile))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
