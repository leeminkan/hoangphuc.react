import React, { Component } from 'react';
import axios from 'axios';
import {api} from '../../config';

import Post from '../../component/Post';



class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: []
        };
    }
    searchPost = async (value) => {
        try {
            let response = await axios.get(api.searchPost,
                {
                    headers: {
                    "Authorization" : `Bearer ${localStorage.getItem('token')}`
                    },
                    params: {
                        key: value
                    }
                });
            this.setState(prevState => ({
                ...prevState,
                posts: response.data.data
                }));
          } catch(error) {
            if (error.response && error.response.data) {
                if (error.response.status === 401)
                {
                    this.logOut();
                }
                else
                {
                    alert(JSON.stringify(error.response.data.errors, null, 1));
                }
              }  else {
                console.log(error);
              }
            };
    }
    renderPost = () =>{
        let posts = this.state.posts;
        return posts.map((post, index) => {
            return (
                <Post
                key = {index}
                name= {post.name}
                created_at= {post.created_at}
                content= {post.content}></Post>
            )
        });
    }
    componentWillMount(){
        if (this.props.location.value)
        {
            this.searchPost(this.props.location.value);
        }
    }
    componentWillReceiveProps(nextProps){
        if (nextProps.location.value)
        {
            this.searchPost(nextProps.location.value);
        }
    }
    render(){
            return (
                <div className="container">
                    <div className="searchPage">
                        <div className="PageSwitcher">Search Page</div>
                        {this.renderPost()}
                    </div>
                </div>
            );
    }
}

export default SearchPage;
