import * as types from './Constants';
export const userPageLoaded = () => ({
    type: types.USER_PAGE_LOADED
});
export const changePassword = (payload) => ({
    type: types.CHANGE_PASSWORD,
    payload: payload
});
export const changePasswordSuccess = (response) => ({
    type: types.CHANGE_PASSWORD_SUCCESS,
    response: response
});
export const changePasswordError = (errors) => ({
    type: types.CHANGE_PASSWORD_ERROR,
    errors: errors
});
export const userPageUnloaded = () => ({
    type: types.USER_PAGE_UNLOADED
});
