import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import actions from '../../redux/actions';
class ListFriends extends Component {
  componentWillMount() {
    this.props.onLoad();
  }
  renderListFriends = () => {
    let friends = this.props.friends;
    return friends.map((friend, index) => {
      return (
        <div key={index} className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div className="friend-card">
              <div className="friend-card-img">
                <img
                  src="https://via.placeholder.com/100.png"
                  alt="avatar"
                />
              </div>
              <div className="friend-card-content">
                <div className="friend-card-name">{friend.name}</div>
                <Link to={`/profile?id=${friend.id}`}>View more...</Link>
              </div>
            </div>
          </div>
      );
    });
  }
  render() {
    return (
      <div className="content">
        <div className="content-header">List Friends</div>
        <div className="row">
          {this.renderListFriends()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  friends: state.friends.listFriends
});
const mapDispatchToProps = dispatch => ({
  onLoad: () =>
  dispatch(actions.getListFriends()),
});
export default connect(mapStateToProps, mapDispatchToProps)(ListFriends);
