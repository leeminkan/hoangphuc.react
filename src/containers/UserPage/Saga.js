import { put, takeLatest, call } from 'redux-saga/effects';

import actions from '../../redux/actions';
import types from '../../redux/constants';

import {api} from '../../config';
import axios from 'axios';

function* catchError(error){
  let errors;
  if (error.response && error.response.data) {
      if (error.response.status === 401)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Token expired!"
          };
          yield put(actions.logOut());
      }
      else
      if (error.response.status === 500)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Error Server!"
          };
      }
      else
      {
        errors = error.response.data.errors;
      }
    }  else {
        errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
    }
    return errors;
}
function* getUserPageFlow(){
  try {
    yield put(actions.getProfile());
    yield put(actions.getMyPosts());
  } catch(error){
    let errors = 
    {
      errorCode: 123456789,
      errorMessage: "Error in Server!"
    };
    yield put(actions.addError(errors));
  };
}
function* unloadUserPageFlow(){
  try {
    yield put(actions.resetPost());
  } catch(error){
    let errors = 
    {
      errorCode: 123456789,
      errorMessage: "Error in Server!"
    };
    yield put(actions.addError(errors));
  };
}
const changePasswordApi = async (payload) => {
  let response = await axios.put(api.changePassword, payload, 
    {
        headers: {
        "Authorization" : `Bearer ${localStorage.getItem('token')}`
        }
    });
  return response;
}

function* changePasswordFlow(action){
  try {
    let response = yield call(changePasswordApi, action.payload);
    yield put(actions.changePasswordSuccess(response.data.data));
    let notification = 
        {
          notificationCode: 123456789,
          notificationMessage: "Change password successfully!"
        };
    yield put(actions.addNotification(notification));
  } catch(error){
    let errors = yield call(catchError, error);
    yield put(actions.changePasswordError(errors));
    yield put(actions.addError(errors));
  };
}
function* actionWatcher() {
  yield takeLatest(types.USER_PAGE_LOADED, getUserPageFlow);
  yield takeLatest(types.USER_PAGE_UNLOADED, unloadUserPageFlow);
  yield takeLatest(types.CHANGE_PASSWORD, changePasswordFlow);
}

export default actionWatcher();