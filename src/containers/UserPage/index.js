import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import UserSidebar from './UserSidebar';
import Profile from '../Profile';
import Posts from '../Posts';
import Friends from './ListFriends';
import ChangePassword from './ChangePassword';

import { connect } from 'react-redux';
import * as actions from './Actions';
import './index.css';


class UserPage extends Component {
    renderPost = () =>{
        return (
            <div className="posts">
                <div className="PageSwitcher">Posts</div>
                <Posts/>
            </div>
        )
    }
    renderProfile = () => {
        return (
            <div className="profile">
                <div className="PageSwitcher">Profile
                </div>
                <Profile/>
            </div>
        );
    }
    componentWillMount() {
        this.props.onLoad();
    }
    componentWillUnmount() {
        this.props.onUnload();
    }
    render(){
            return (
              <div className="container">
                <div className="row user-page">
                  <div className="col-md-3">
                    <UserSidebar
                      name={this.props.user.name}
                      job="Developer"
                    />
                  </div>
                  <div className="col-md-9">
                      <Switch>
                        <Route
                          exact
                          path="/user"
                          component={Posts}
                        />
                        <Route
                          exact
                          path="/user/profile"
                          component={Profile}
                        />
                        <Route
                          exact
                          path="/user/change-password"
                          component={ChangePassword}
                        />
                        <Route
                          exact
                          path="/user/friends"
                          component={Friends}
                        />
                      </Switch>
                    </div>
                  </div>
                </div>
            );
    }
}
const mapStateToProps = state => ({
    page: state.userPage,
    user: state.profile.user
});
const mapDispatchToProps = dispatch => ({
    onLoad: () =>
    dispatch(actions.userPageLoaded()),
    onUnload: () =>
    dispatch(actions.userPageUnloaded())
  });
export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
