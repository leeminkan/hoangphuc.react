import React, { Component, Fragment } from 'react';

import LoadingSpinner from '../../component/LoadingSpinner';

import { connect } from 'react-redux';
import actions from '../../redux/actions';

import ChangePasswordForm from '../../component/Form/ChangePasswordForm';

class ChangePassword extends Component {
  onSubmit = values => {
    this.props.onSubmit(values);
  };
  render() {
    const { inProgress } = this.props;
    return (
      <Fragment>
        <h3 className="login-heading mb-4 text-center">Change Password!</h3>
        <ChangePasswordForm
          onSubmit={this.onSubmit}
        />
        {inProgress ? <LoadingSpinner /> : null }
      </Fragment>
    );
  }
}
const mapDispatchToProps = dispatch => ({
    onSubmit: (payload) =>
    dispatch(actions.changePassword(payload))
}
);
export default connect(null, mapDispatchToProps)(ChangePassword);