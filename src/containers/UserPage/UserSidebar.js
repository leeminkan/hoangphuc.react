import React, { Component } from "react";
import { Link } from "react-router-dom";
class UserSidebar extends Component {
  render() {
    return (
      <div className="profile-sidebar">
        <div className="profile-userpic">
          <img src="/img/4.jpg" className="img-responsive" alt="avatar" />
        </div>
        <div className="profile-usertitle">
          <div className="profile-usertitle-name">{this.props.name}</div>
          <div className="profile-usertitle-job">{this.props.job}</div>
        </div>
        <div className="profile-menu">
          <Link className="btn btn-block btn-light" to="/user">
            Posts
          </Link>
          <Link className="btn btn-block btn-light" to="/user/profile">
            Profile
          </Link>
          <Link className="btn btn-block btn-light" to="/user/change-password">
            Change Password
          </Link>
          <Link className="btn btn-block btn-light" to="/user/friends">
            Friends
          </Link>
        </div>
      </div>
    );
  }
}

export default UserSidebar;
