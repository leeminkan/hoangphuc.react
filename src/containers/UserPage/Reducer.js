import * as types from './Constants';

var initialState = {
    errors: [],
    inProgress: false,
}
const myReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.USER_PAGE_LOADED:
            return { ...state };
        case types.CHANGE_PASSWORD:
            return { ...state,
                inProgress: {
                    changePassword: true
                }
            };
        case types.CHANGE_PASSWORD_SUCCESS:
            return { ...state,
                inProgress: false,
            };
        case types.CHANGE_PASSWORD_ERROR:
            return { ...state,
                inProgress: false,
                errors: action.errors
            };
        case types.USER_PAGE_UNLOADED:
            return { ...initialState };
        default:
            return state;
    }
}

export default myReducer;