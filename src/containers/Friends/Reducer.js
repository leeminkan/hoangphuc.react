import * as types from './Constants';

var initialState = {
    listFriends: [],
    inProgress: false,
    errors: []
}
const myReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_LIST_FRIENDS:
      return {
        ...state,
        inProgress: {
          getNewfeed: true
        }
      };
    case types.GET_LIST_FRIENDS_SUCCESS:
      return { ...state, listFriends: action.listFriends, inProgress: false };
    case types.GET_LIST_FRIENDS_ERROR:
      return { ...state, errors: action.errors, inProgress: false };

    default:
      return state;
  }
};

export default myReducer;