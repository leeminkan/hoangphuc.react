import { put, call, takeLatest } from 'redux-saga/effects';

import actions from '../../redux/actions';
import types from '../../redux/constants';

import {api} from '../../config';
import axios from 'axios';


function* catchError(error){
  let errors;
  if (error.response && error.response.data) {
      if (error.response.status === 401)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Token expired!"
          };
          yield put(actions.logOut());
      }
      else
      if (error.response.status === 500)
      {
          errors = 
          {
            errorCode: 123456789,
            errorMessage: "Error Server!"
          };
      }
      else
      {
        errors = error.response.data.errors;
      }
    }  else {
        errors = 
        {
          errorCode: 123456789,
          errorMessage: "Error in Server!"
        };
    }
    return errors;
}
const getListFriendsApi = async () => {
  let response = await axios.get(api.listFriends,
    {
        headers: {
        "Authorization" : `Bearer ${localStorage.getItem('token')}`
        }
    });
  return response;
}

function* getListFriendsFlow(){
    try {
        let response = yield call(getListFriendsApi);
        yield put(actions.getListFriendsSuccess(response.data.data));
      } catch(error){
        let errors = yield call(catchError, error);
        yield put(actions.getListFriendsError(errors));
        yield put(actions.addError(errors));
      };
}

function* actionWatcher() {
  yield takeLatest(types.GET_LIST_FRIENDS, getListFriendsFlow);
}

export default actionWatcher();