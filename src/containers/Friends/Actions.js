import * as types from './Constants';
export const getListFriends = () => ({
    type: types.GET_LIST_FRIENDS
});
export const getListFriendsSuccess = (listFriends) => ({
    type: types.GET_LIST_FRIENDS_SUCCESS,
    listFriends: listFriends
});
export const getListFriendsError = (errors) => ({
    type: types.GET_LIST_FRIENDS_ERROR,
    errors: errors
});
