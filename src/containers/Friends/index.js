import React, { Component } from 'react';
import UserSidebar from '../UserPage/UserSidebar';

import { connect } from 'react-redux';
import * as actions from './Actions';


class FriendPage extends Component {
    componentWillMount() {
        this.props.onLoad();
    }
    render(){
      let params = new URLSearchParams(this.props.location.search);
      let id = params.get('name');
            return (
              <div className="container">
                <div className="row user-page">
                  <div className="col-md-3">
                    <UserSidebar
                      name="Friend"
                      job="Developer"
                    />
                  </div>
                  <div className="col-md-9">
                      {id}
                    </div>
                  </div>
                </div>
            );
    }
}
const mapStateToProps = state => ({
    page: state.userPage,
    user: state.profile.user
});
const mapDispatchToProps = dispatch => ({
    onLoad: () =>
    dispatch(actions.getListFriends()),
  });
export default connect(mapStateToProps, mapDispatchToProps)(FriendPage);
